# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :home_visit_service,
  ecto_repos: [HomeVisitService.Repo]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :email_checker,
  validations: [EmailChecker.Check.Format]

config :home_visit_service, :tasks, [
  %{
    id: 1,
    short_name: "companionship",
    description: "spend time with member"
  },
  %{
    id: 2,
    short_name: "chore",
    description: "do common household chore for member"
  },
  %{
    id: 3,
    short_name: "errand",
    description: "run errand for member"
  }
]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
