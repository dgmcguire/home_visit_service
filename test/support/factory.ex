defmodule Test.Support.Factory do
  @moduledoc """
  public API for all things factory related.
  """

  alias Test.Support.Factory.User
  alias Test.Support.Factory.Visit

  def to_params(struct) do
    struct
    |> Map.from_struct()
    |> Map.delete(:__meta__)
  end

  def build(model, override_params \\ %{})
  def build(:user, override_params), do: User.build(override_params)
  def build(:visit, override_params), do: Visit.build(override_params)
end
