defmodule Test.Support.Factory.User do
  @moduledoc false

  alias HomeVisitService.User.Schema, as: User

  def build(override_params) do
    %User{
      email: "test@email.com",
      first_name: "firstname",
      last_name: "lastname",
      is_member: false,
      is_pal: false,
      minutes_balance: 100
    }
    |> struct(override_params)
  end
end
