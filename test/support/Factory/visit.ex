defmodule Test.Support.Factory.Visit do
  @moduledoc false

  alias HomeVisitService.Visit.Schema

  def build(override_params) do
    %Schema{
      minutes: 60,
      tasks: [1, 2]
    }
    |> struct(override_params)
  end
end
