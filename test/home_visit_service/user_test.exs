defmodule Test.HomeVisitService.UserTest do
  use HomeVisitService.DataCase

  alias HomeVisitService.User
  alias HomeVisitService.User.Schema, as: UserSchema
  alias Test.Support.Factory
  alias Ecto.Changeset

  test "can successfully create a user" do
    {:ok, user} =
      Factory.build(:user, %{balance_minutes: 50})
      |> Factory.to_params()
      |> User.create()

    assert user.id != nil
  end

  test "can validate a users email" do
    {:error, %Changeset{valid?: is_valid}} =
      Factory.build(:user, %{email: "invalid_email"})
      |> Factory.to_params()
      |> User.create()

    assert is_valid == false

    {:ok, %UserSchema{}} =
      Factory.build(:user, %{email: "valid@email.com"})
      |> Factory.to_params()
      |> User.create()
  end
end
