defmodule Test.HomeVisitService.VisitTest do
  use HomeVisitService.DataCase

  alias Test.Support.Factory
  alias HomeVisitService.Repo
  alias HomeVisitService.Visit
  alias HomeVisitService.Visit.Schema, as: VisitSchema
  alias HomeVisitService.User
  alias HomeVisitService.User.Schema, as: UserSchema

  test "members can request a visit with task_ids, email and date" do
    task_ids = [1, 2, 3]
    email = "member@email.com"
    date = NaiveDateTime.utc_now()

    {:ok, %{id: member_id}} =
      Factory.build(:user, %{
        is_member: true,
        email: email,
        minutes_balance: 100
      })
      |> Factory.to_params()
      |> User.create()

    {:ok, visit} =
      Factory.build(:visit, %{
        schedule_date: date,
        tasks: task_ids,
        minutes: 99
      })
      |> Map.from_struct()
      |> Map.merge(%{member_email: email})
      |> Visit.request()

    [user] = Repo.all(UserSchema)
    assert user.minutes_balance == 1
    assert visit.state == "requested"
    assert visit.member_id == member_id
    assert Repo.all(VisitSchema) |> length() == 1
  end

  test "members without enough balance can't request visit" do
    task_ids = [1, 2, 3]
    email = "member@email.com"
    date = NaiveDateTime.utc_now()
    minutes_balance = 59
    request_minutes = 60

    Factory.build(:user, %{
      is_member: true,
      email: email,
      minutes_balance: minutes_balance
    })
    |> Factory.to_params()
    |> User.create()

    {:error, errors} =
      Factory.build(:visit, %{
        schedule_date: date,
        tasks: task_ids,
        minutes: request_minutes
      })
      |> Map.from_struct()
      |> Map.merge(%{member_email: email})
      |> Visit.request()

    assert [minutes: _] = errors
    assert Repo.all(VisitSchema) == []
  end

  test "non-member can't request a visit" do
    non_member_email = "nonmember@email.com"

    Factory.build(:user, %{
      is_member: false,
      email: non_member_email
    })
    |> Factory.to_params()
    |> User.create()

    {:error, error_tag} =
      Factory.build(:visit)
      |> Map.from_struct()
      |> Map.merge(%{member_email: non_member_email})
      |> Visit.request()

    assert error_tag == :member_not_exist
    assert Repo.all(VisitSchema) == []
  end

  test "pal can confirm a requested visit with email and visit_id" do
    email = "pal@email.com"
    {:ok, %VisitSchema{id: visit_id}} = create_visit_request()

    Factory.build(:user, %{is_pal: true, email: email})
    |> Factory.to_params()
    |> User.create()

    {:ok, visit} =
      Factory.build(:visit)
      |> Map.from_struct()
      |> Map.merge(%{pal_email: email, visit_id: visit_id})
      |> Visit.confirm()

    assert Repo.all(VisitSchema) |> length() == 1
    assert visit.state == "confirmed"
  end

  test "non-pal can't comfirm visit" do
    non_pal_email = "nonpal@email.com"
    {:ok, %VisitSchema{id: visit_id}} = create_visit_request()

    Factory.build(:user, %{
      is_pal: false,
      email: non_pal_email
    })
    |> Factory.to_params()
    |> User.create()

    {:error, error_tag} =
      %{
        pal_email: non_pal_email,
        visit_id: visit_id
      }
      |> Visit.confirm()

    [visit | rest] = Repo.all(VisitSchema)
    assert error_tag == :pal_not_exist
    assert rest == []
    assert visit.state == "requested"
  end

  test "can't confirm visit in non-requested state" do
    email = "pal@email.com"

    date =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.add(5, :day)
      |> NaiveDateTime.truncate(:second)

    {:ok, %VisitSchema{id: visit_id}} =
      Factory.build(:visit, %{
        state: "invalid",
        schedule_date: date
      })
      |> Repo.insert()

    Factory.build(:user, %{is_pal: true, email: email})
    |> Factory.to_params()
    |> User.create()

    {:error, errors} =
      Factory.build(:visit)
      |> Map.from_struct()
      |> Map.merge(%{pal_email: email, visit_id: visit_id})
      |> Visit.confirm()

    [visit | rest] = Repo.all(VisitSchema)
    assert rest == []
    assert visit.state == "invalid"
    [state: _] = errors
  end

  test "pal can't confirm past schedule_date" do
    email = "pal@email.com"

    schedule_date =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.add(-1, :day)
      |> NaiveDateTime.truncate(:second)

    {:ok, %VisitSchema{id: visit_id}} = create_visit_request(schedule_date)

    Factory.build(:user, %{is_pal: true, email: email})
    |> Factory.to_params()
    |> User.create()

    {:error, errors} =
      Factory.build(:visit)
      |> Map.from_struct()
      |> Map.merge(%{pal_email: email, visit_id: visit_id})
      |> Visit.confirm()

    [visit | rest] = Repo.all(VisitSchema)
    assert rest == []
    assert visit.state == "requested"
    [schedule_date: _] = errors
  end

  test "can complete confirmed visit with visit_id" do
    email = "pal@email.com"
    initial_pal_minutes = 100
    visit_minutes = 100

    {:ok, pal} =
      Factory.build(:user, %{
        is_pal: true,
        email: email,
        minutes_balance: initial_pal_minutes
      })
      |> Factory.to_params()
      |> User.create()

    date =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.add(5, :day)
      |> NaiveDateTime.truncate(:second)

    {:ok, %VisitSchema{id: visit_id}} =
      Factory.build(:visit, %{
        state: "confirmed",
        schedule_date: date,
        minutes: visit_minutes,
        pal_id: pal.id
      })
      |> Repo.insert()

    {:ok, visit} =
      Factory.build(:visit)
      |> Map.from_struct()
      |> Map.merge(%{visit_id: visit_id})
      |> Visit.complete()

    pal = Repo.one(from u in UserSchema, where: u.id == ^visit.pal_id)
    assert pal.minutes_balance == initial_pal_minutes + 85
    assert Repo.all(VisitSchema) |> length() == 1
  end

  defp create_visit_request(schedule_date \\ nil) do
    date =
      case schedule_date do
        nil ->
          NaiveDateTime.utc_now()
          |> NaiveDateTime.add(5, :day)
          |> NaiveDateTime.truncate(:second)

        date ->
          date
      end

    task_ids = [1, 2]

    {:ok, %{id: member_id}} =
      Factory.build(:user, %{is_member: true})
      |> Factory.to_params()
      |> User.create()

    Factory.build(:visit, %{
      member_id: member_id,
      schedule_date: date,
      tasks: task_ids
    })
    |> VisitSchema.request_changeset()
    |> Repo.insert()
  end
end
