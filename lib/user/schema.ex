defmodule HomeVisitService.User.Schema do
  @moduledoc """
  Schema and validations for the User model.
  """

  use Accessible
  use Ecto.Schema

  alias Ecto.Changeset

  schema "users" do
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :minutes_balance, :integer, default: 0

    field :is_member, :boolean, default: false
    field :is_pal, :boolean, default: false

    timestamps()
  end

  @required ~w|email first_name last_name|a
  @allowed ~w|is_pal is_member minutes_balance|a ++ @required

  def changeset(user = %__MODULE__{}, params \\ %{}) do
    user
    |> Changeset.cast(params, @allowed)
    |> Changeset.validate_required(@required)
    |> Changeset.unique_constraint([:email])
    |> validate_email(:email)
  end

  def deduct_changeset(user = %__MODULE__{}, %{deductable_minutes: minutes}) do
    new_balance = user.minutes_balance - minutes
    Changeset.change(user, %{minutes_balance: new_balance})
  end

  def increment_changeset(user = %__MODULE__{}, %{completed_minutes: minutes}) do
    minutes_to_add = (minutes * 0.85) |> trunc()
    new_balance = user.minutes_balance + minutes_to_add
    Changeset.change(user, %{minutes_balance: new_balance})
  end

  defp validate_email(changeset, field) do
    changeset
    |> Changeset.validate_change(field, fn ^field, email ->
      case EmailChecker.valid?(email) do
        true -> []
        _ -> [email: "is invalid"]
      end
    end)
  end
end
