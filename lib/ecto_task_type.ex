defmodule HomeVisitService.EctoTaskType do
  @moduledoc """
  serialize tasks in and out of binary representation.
  Just a fun way to handle many_to_many relationships with
  a database that doesn't support collection field types, while
  also avoiding creating extra tables.

  The biggest drawback here would be binary data isn't human
  readable from something like a sql client. So you'd need
  to pass the data through your code to know what you're 
  looking at.
  """

  use Ecto.Type

  @task_ids :home_visit_service
            |> Application.compile_env(:tasks)
            |> Enum.map(& &1.id)

  @tasks Application.compile_env(:home_visit_service, :tasks)

  def type, do: :binary

  def cast([]), do: {:error, [message: "must provide a task"]}

  def cast(list) when is_list(list) do
    not_unique = Enum.uniq(list) != list
    not_defined = list -- @task_ids != []

    cond do
      not_unique ->
        {:error, [message: "can't contain duplicate tasks"]}

      not_defined ->
        {:error, [message: "id must be in defined id's: #{inspect(@task_ids)}"]}

      true ->
        {:ok, list}
    end
  end

  def cast(_) do
    {:error, [message: "must be a list"]}
  end

  def load(binary) when is_binary(binary) do
    tasks =
      binary
      |> :erlang.binary_to_term()
      |> Enum.map(&Enum.find(@tasks, fn task -> task.id == &1 end))

    {:ok, tasks}
  end

  def dump(term) do
    {:ok, :erlang.term_to_binary(term)}
  end
end
