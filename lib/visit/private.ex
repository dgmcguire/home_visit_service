defmodule HomeVisitService.Visit.Private do
  @moduledoc """
  Any non-trivial functionality for the visit model goes here.
  """

  import Ecto.Query

  alias Ecto.Multi
  alias HomeVisitService.User.Schema, as: UserSchema
  alias HomeVisitService.Repo
  alias HomeVisitService.Visit.Schema, as: VisitSchema

  def request(
        params = %{
          member_email: member_email,
          schedule_date: schedule_date
        }
      ) do
    Multi.new()
    |> Multi.one(:member, get_user_query(member_email, :member))
    |> Multi.run(:is_member, fn _repo, %{member: member} ->
      if member && member.is_member, do: {:ok, member}, else: {:error, :member_not_exist}
    end)
    |> Multi.insert(:visit, fn %{member: member} ->
      params =
        params
        |> Map.merge(%{
          member_id: member.id,
          schedule_date: schedule_date,
          user_minutes_balance: member.minutes_balance
        })

      %VisitSchema{}
      |> VisitSchema.request_changeset(params)
    end)
    |> Multi.update(:deduct_minutes, fn %{member: member, visit: visit} ->
      UserSchema.deduct_changeset(member, %{deductable_minutes: visit.minutes})
    end)
    |> Repo.transaction()
    |> then(fn
      {:ok, %{visit: visit}} -> {:ok, visit}
      {:error, :is_member, error_tag, _multi_state} -> {:error, error_tag}
      {:error, :visit, changeset, _multi_state} -> {:error, changeset.errors}
    end)
  end

  def confirm(%{pal_email: pal_email, visit_id: visit_id}) do
    Multi.new()
    |> Multi.one(:pal, get_user_query(pal_email, :pal))
    |> Multi.run(:is_pal, fn _repo, %{pal: pal} ->
      if pal && pal.is_pal do
        {:ok, pal}
      else
        {:error, :pal_not_exist}
      end
    end)
    |> Multi.one(:visit, get_visit_query(visit_id))
    |> Multi.update(:update, fn %{visit: visit, pal: pal} ->
      visit
      |> VisitSchema.confirm_changeset(%{pal_id: pal.id})
    end)
    |> Repo.transaction()
    |> then(fn
      {:ok, %{update: visit}} ->
        {:ok, visit}

      {:error, :is_pal, error_tag, _multi_state} ->
        {:error, error_tag}

      {:error, :update, changeset, _multi_state} ->
        {:error, changeset.errors}
    end)
  end

  def complete(%{visit_id: visit_id}) do
    Multi.new()
    |> Multi.one(:visit, get_visit_query(visit_id))
    |> Multi.one(:pal, fn %{visit: visit} ->
      from u in UserSchema, where: u.id == ^visit.pal_id
    end)
    |> Multi.update(:update, fn %{visit: visit} ->
      visit
      |> VisitSchema.complete_changeset()
    end)
    |> Multi.update(:add_minutes, fn %{pal: pal, visit: visit} ->
      UserSchema.increment_changeset(pal, %{completed_minutes: visit.minutes})
    end)
    |> Repo.transaction()
    |> then(fn
      {:ok, %{update: visit}} -> {:ok, visit}
    end)
  end

  defp get_user_query(email, :member) do
    from(u in UserSchema,
      where: u.email == ^email,
      where: u.is_member == true
    )
  end

  defp get_user_query(email, :pal) do
    from(u in UserSchema,
      where: u.email == ^email,
      where: u.is_pal == true
    )
  end

  defp get_visit_query(visit_id) do
    from(v in VisitSchema, where: v.id == ^visit_id)
  end
end
