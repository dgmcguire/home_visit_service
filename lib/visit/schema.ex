defmodule HomeVisitService.Visit.Schema do
  @moduledoc """
  Schema and validations for the Visit model.
  """

  use Accessible
  use Ecto.Schema

  alias Ecto.Changeset
  alias HomeVisitService.User.Schema, as: UserSchema
  alias HomeVisitService.EctoTaskType

  schema "visits" do
    field :minutes, :integer
    field :description, :string
    field :state, :string
    field :tasks, EctoTaskType
    field :schedule_date, :naive_datetime

    belongs_to :member, UserSchema
    belongs_to :pal, UserSchema

    timestamps()
  end

  @base_allowed ~w|description minutes tasks|a

  @request_required ~w|member_id schedule_date|a
  @request_allowed @base_allowed ++ @request_required

  @confirm_required ~w|pal_id|a
  @confirm_allowed ~w|pal_id schedule_date|a ++ @base_allowed

  @visit_states ~w|requested confirmed completed|

  def request_changeset(visit = %__MODULE__{}, params \\ %{}) do
    {user_minutes_balance, params} = Map.pop(params, :user_minutes_balance)

    visit
    |> Changeset.cast(params, @request_allowed)
    |> Changeset.validate_required(@request_required)
    |> validate_enough_minutes_balance(user_minutes_balance)
    |> set_state("requested")
  end

  def confirm_changeset(visit = %__MODULE__{}, params \\ %{}) do
    visit
    |> Changeset.cast(params, @confirm_allowed)
    |> Changeset.validate_required(@confirm_required)
    |> validate_state("requested")
    |> validate_schedule_in_future()
    |> set_state("confirmed")
    |> update_timestamp()
  end

  def complete_changeset(visit) do
    visit
    |> Changeset.change()
    |> validate_state("confirmed")
    |> set_state("completed")
    |> update_timestamp()
  end

  defp update_timestamp(changeset) do
    updated_time =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.truncate(:second)

    Changeset.put_change(changeset, :updated_at, updated_time)
  end

  defp validate_enough_minutes_balance(changeset, balance) do
    request_minutes = Changeset.get_field(changeset, :minutes)

    if request_minutes > balance do
      Changeset.add_error(
        changeset,
        :minutes,
        "requested #{request_minutes} minutes with only #{balance} minutes remaining"
      )
    else
      changeset
    end
  end

  defp validate_state(changeset, expected_state) do
    next_state =
      case expected_state do
        "requested" -> "confirmed"
        "confirmed" -> "completed"
      end

    changeset
    |> Changeset.get_field(:state)
    |> case do
      ^expected_state ->
        changeset

      _ ->
        Changeset.add_error(
          changeset,
          :state,
          "can't #{next_state} visits not in #{expected_state} state"
        )
    end
  end

  defp validate_schedule_in_future(changeset) do
    Changeset.get_field(changeset, :schedule_date)
    |> NaiveDateTime.compare(NaiveDateTime.utc_now())
    |> case do
      :lt ->
        Changeset.add_error(changeset, :schedule_date, "can't confirm visit in the past")

      _ ->
        changeset
    end
  end

  defp set_state(changeset, state) when state in @visit_states do
    Changeset.put_change(changeset, :state, state)
  end

  defp set_state(changeset, _state) do
    Changeset.add_error(changeset, :state, "not an allowed state")
  end
end
