defmodule HomeVisitService.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      HomeVisitService.Repo
    ]

    opts = [strategy: :one_for_one, name: HomeVisitService.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
