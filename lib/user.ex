defmodule HomeVisitService.User do
  @moduledoc """
  Public API for User.
  All user functionality should be exposed through this module.
  """

  alias HomeVisitService.Repo
  alias HomeVisitService.User.Schema

  def create(params) do
    %Schema{}
    |> Schema.changeset(params)
    |> Repo.insert()
  end
end
