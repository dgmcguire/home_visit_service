defmodule HomeVisitService.Visit do
  @moduledoc """
  Public API for Visit.
  All visit functionality should be exposed through this module.
  """

  alias HomeVisitService.Visit.Private

  def request(params = %{member_email: _, schedule_date: _}) do
    Private.request(params)
  end

  def confirm(params = %{pal_email: _, visit_id: _}) do
    Private.confirm(params)
  end

  def complete(params = %{visit_id: _}) do
    Private.complete(params)
  end
end
