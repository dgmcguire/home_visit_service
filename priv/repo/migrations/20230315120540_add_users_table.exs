defmodule HomeVisitService.Repo.Migrations.AddUsersTable do
  use Ecto.Migration

  def change do
    create table("users") do
      add :email, :string
      add :first_name, :string
      add :last_name, :string

      add :is_member, :boolean, default: false
      add :is_pal, :boolean, default: false
      add :minutes_balance, :integer, default: 0

      timestamps()
    end
  end
end
