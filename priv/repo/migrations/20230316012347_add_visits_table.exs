defmodule HomeVisitService.Repo.Migrations.AddVisitsTable do
  use Ecto.Migration

  def change do
    create table("visits") do
      add :minutes, :integer
      add :tasks, :binary
      add :description, :string
      add :state, :string
      add :schedule_date, :naive_datetime

      add :member_id, references("users")
      add :pal_id, references("users")

      timestamps()
    end
  end
end
