# HomeVisitService

The API's functionality and intent
is (attempted to be) made clear by the models public API's as well as the tests.

## External Dependencies

You will need sqlite3 to run the tests on this project.

## Setup

To run the tests

```
MIX_ENV=test mix do deps.get, compile, ecto.setup, test
```

